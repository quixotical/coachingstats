# README #

This is a simple application that I am making to help coaches record progress of their players.

### How do I get set up? ###

* For this app I am using mySQL and Swift. 

#### Dependencies ####
* Charts
* Alamofire
* SimpleKeychain
* SwiftyJSON

### Contribution guidelines ###

* I am currently the sole contributor for this project

### Who do I talk to? ###

* Owned by JCBoomgaarden@gmail.com