//
//  OptionsTableViewCell.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {
    //MARK: - Properties
    @IBOutlet weak var optionName: UILabel!
    @IBOutlet weak var selectedInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
