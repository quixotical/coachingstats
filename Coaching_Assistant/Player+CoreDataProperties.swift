//
//  Player+CoreDataProperties.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/10/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Player {

    @NSManaged var name: String?
    @NSManaged var id: NSNumber

}
