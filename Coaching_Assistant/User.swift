//
//  User.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/14/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import Foundation
import CoreData


class User: NSManagedObject {
    
    convenience init?(email: String, id: NSNumber, sessionExists: NSNumber, teams: NSSet){
        self.init()
        self.email = email
        self.id = id
        self.sessionExists = sessionExists
        self.teams = teams
        
        if email.isEmpty || id.integerValue <= 0{
            return nil
        }
    }

}
