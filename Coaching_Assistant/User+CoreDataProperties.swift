//
//  User+CoreDataProperties.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/14/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var email: String?
    @NSManaged var id: NSNumber?
    @NSManaged var sessionExists: NSNumber?
    @NSManaged var teams: NSSet?

}
