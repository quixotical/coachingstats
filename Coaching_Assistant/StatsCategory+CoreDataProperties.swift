//
//  StatsCategory+CoreDataProperties.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/21/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension StatsCategory {

    @NSManaged var id: NSNumber?
    @NSManaged var team_id: NSNumber?
    @NSManaged var category: String?

}
