//
//  ViewController.swift
//  Volleyball_Stats_Tracker
//
//  Created by Jacob Boomgaarden on 4/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SwiftyJSON
import SimpleKeychain

class LoginViewController: UIViewController {
    //MARK: Properties
    @IBOutlet weak var tbEmail: UITextField!
    @IBOutlet weak var tbPassword: UITextField!
    var user : User?
    var managedContext : NSManagedObjectContext? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.checkUserLoggedIn()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Segue section
    @IBAction func unwindToLoginSegue(sender: UIStoryboardSegue){
        if let sourceViewController = sender.sourceViewController as? NewUserViewController{
            print("coming back from new user")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToTeams" {
           if let user = self.user{
                let navController = segue.destinationViewController as! UINavigationController
                let welcomeView = navController.topViewController as! TeamsTableViewController
                welcomeView.user = user
           }else{
            let errorLoggingInAlert = UIAlertController(title: "Oops!", message: "It looks like there was an error logging in", preferredStyle: UIAlertControllerStyle.Alert)
            errorLoggingInAlert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler:self.returnToLogin))
            self.presentViewController(errorLoggingInAlert, animated: true, completion: nil)
            }
        }
    }
    //MARK: Login logic
    
    //go to welcome screen if user logged in
    func checkUserLoggedIn(){
        managedContext = getManagedContext()

        let fetchRequest = NSFetchRequest(entityName: "User")
        let sessionExistsPredicate = NSPredicate(format: "sessionExists == TRUE")
        fetchRequest.predicate = sessionExistsPredicate
        
        if self.retrieveLoggedInUser(fetchRequest) != nil{
          let storyBoard = UIStoryboard(name: "Main", bundle:nil)
          self.goToTeamsView(storyBoard)
        }
    }
    
    //send the user to the Teams Welcome View Controller
    func goToTeamsView(storyBoard : UIStoryboard){
        
        let welcomeView = storyBoard.instantiateViewControllerWithIdentifier("teamsView")
        
        let navigationController = UINavigationController(rootViewController: welcomeView)
        super.performSegueWithIdentifier("segueToTeams", sender: self)
        super.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    //MARK: Actions
    @IBAction func loginButton(sender: UIButton) {
        let parameters = [
            "email": tbEmail.text as! AnyObject,
            "password": tbPassword.text as! AnyObject
        ]
        Alamofire.request(.POST, "http://localhost:8000/api/login", parameters: parameters, encoding: .JSON)
            .response{ request, response, data, error in
                if response?.statusCode == 200{
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle:nil)
                    
                    let userData = JSON(data : data!).dictionary
                    let userWithToken = userData!["data"]?.dictionary
                    let retrievedUser = userWithToken!["user"]?.dictionary
                    let token = userWithToken!["token"]?.string
                    let email = retrievedUser!["email"]?.string
                    let id = retrievedUser!["id"]?.int
                    
                    self.storeUserToken(token!)
                    
                    self.managedContext = self.getManagedContext()
                    
                    let fetchRequest = NSFetchRequest(entityName: "User")
                    let emailPredicate = NSPredicate(format: "email == %@", email!)
                    fetchRequest.predicate = emailPredicate
                    
                    if let user = self.retrieveLoggedInUser(fetchRequest){
                        user.setValue(true, forKey: "sessionExists")
                        self.user = user
                        do{
                            try self.managedContext!.save()
                            self.goToTeamsView(storyBoard)
                        }catch let error as NSError{
                            print("Could not fetch \(error), \(error.userInfo)")
                        }
                    }else{
                        self.saveUser(email!, id: id!, storyBoard: storyBoard)
                    }
                //401 is incorrect username/password
                }else if response?.statusCode == 401{
                    let incorrectPasswordAlert = UIAlertController(title: "Oops!", message: "Username or password incorrect", preferredStyle: UIAlertControllerStyle.Alert)
                    incorrectPasswordAlert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler:nil))
                    self.presentViewController(incorrectPasswordAlert, animated: true, completion: nil)
                //Generic error message for all other errors
                }else{
                    print(response?.statusCode)
                    let genericErrorAlert = UIAlertController(title: "Oops!", message: "An error occurred while attempting to log you in. Please try again", preferredStyle: UIAlertControllerStyle.Alert)
                    genericErrorAlert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(genericErrorAlert, animated: true, completion: nil)
                }
        }
    }
    
    func getManagedContext()-> NSManagedObjectContext{
        if managedContext == nil{
          let appDelegate =
              UIApplication.sharedApplication().delegate as! AppDelegate
        
          managedContext = appDelegate.managedObjectContext
          return managedContext!
        }
        return managedContext!
    }
    
    //check if user is logged in and return the active user
    func retrieveLoggedInUser(fetchRequest : NSFetchRequest)-> User?{
        do{
            let results = try managedContext!.executeFetchRequest(fetchRequest) as! [NSManagedObject]
            if results.count > 0{
                user = (results[0] as? User)!
                return user
            }
        }catch let error as NSError{
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
    func returnToLogin(action: UIAlertAction) -> Void {
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)

        let loginView = storyBoard.instantiateViewControllerWithIdentifier("loginView")
        
        let navigationController = UINavigationController(rootViewController: loginView)
        
        super.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    func storeUserToken(token : String) -> Void{
        let jwt = token
        let instance = A0SimpleKeychain()
        instance.setString(jwt, forKey: "userTokenKey")
    }
    
    func saveUser(email: String, id : Int, storyBoard : UIStoryboard){
        let entity =  NSEntityDescription.entityForName("User",
                                                        inManagedObjectContext:self.managedContext!)
        let newUser = NSManagedObject(entity: entity!,
                                      insertIntoManagedObjectContext: self.managedContext!)
        newUser.setValue(email, forKey: "email")
        newUser.setValue(id, forKey: "id")
        newUser.setValue(true, forKey: "sessionExists")
        
        do {
            try self.managedContext!.save()

            self.user = newUser as? User
            self.goToTeamsView(storyBoard)
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }

    }
}

