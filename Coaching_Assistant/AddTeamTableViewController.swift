//
//  AddTeamTableViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SimpleKeychain
import SwiftyJSON

class AddTeamTableViewController: UITableViewController {
    //MARK: - Properties
    var teams = [Teams]()
    var user : User?
    var addTeamOptions = []
    let nameOption = "Team Name" 
    let sportOption = "Sport Type"
    let statsToTrackOption = "Stats to Track"
    
    var teamName : String?
    var sportName : String?
    var newStatCategories : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAddTeamOptions()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func loadAddTeamOptions(){
        addTeamOptions = [nameOption, sportOption, statsToTrackOption]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return addTeamOptions.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "tableCellOptionsIdentifier"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! OptionsTableViewCell
        let addTeamOption = addTeamOptions[indexPath.row]
        cell.optionName.text = addTeamOption as? String
//        cell.selectedInfo.hidden = true
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        switch addTeamOption as! String {
        case "Team Name":
            cell.selectedInfo.text = teamName
        case "Sport Type":
            cell.selectedInfo.text = sportName
        case "Stats to Track":
            if newStatCategories.count > 0{
                cell.selectedInfo.text = String(newStatCategories.count)
            }else{
                cell.selectedInfo.text = ""
            }
        default:
            cell.selectedInfo.text = "We encountered an error"
            
        }
        if addTeamOption as! String == "Team Name"{
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

            switch addTeamOptions[indexPath.row] as! String {
            case nameOption:
                let teamNameIdentifier = "addTeamNameIdentifier"
                let teamNameVC = self.storyboard?.instantiateViewControllerWithIdentifier(teamNameIdentifier) as? AddTeamNameViewController
                teamNameVC?.selectedOption = nameOption
                self.navigationController?.pushViewController(teamNameVC!, animated: true)
            case sportOption:
                let categoryIdentifier = "selectTeamTableIdentity"
                let categoryTableVC = self.storyboard?.instantiateViewControllerWithIdentifier(categoryIdentifier) as?  SelectTeamTypeTableViewController
                self.navigationController?.pushViewController(categoryTableVC!, animated: true)
            case statsToTrackOption:
                let statsIdentifier = "addNewStats"
                let teamStatsVC = self.storyboard?.instantiateViewControllerWithIdentifier(statsIdentifier) as? AddTeamStatsViewController
                self.navigationController?.pushViewController(teamStatsVC!, animated: true)
            default:
                print("Something is horribly wrong")
            }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "optionSelectedSegue"{
            let addTeamOptionsVC = segue.destinationViewController as? AddTeamNameViewController
            if let selectedOptionCell = sender as? OptionsTableViewCell{
                let indexPath = tableView.indexPathForCell(selectedOptionCell)
                let selectedOption = addTeamOptions[indexPath!.row]
                addTeamOptionsVC?.selectedOption = selectedOption as? String
                
            }
        }
    }
    //MARK: - Actions
    @IBAction func addNewTeamButton(sender: AnyObject) {
        let entity = NSEntityDescription.entityForName("Teams", inManagedObjectContext: self.getManagedContext())
        let newTeam = setNewTeam(entity!)
        postNewTeam(newTeam)
    }
    
    //MARK: - AlamoFire HTTP requests
    func postNewTeam(newTeam : Teams){
        let parameters = [
            "email" : newTeam.user_email as! AnyObject,
            "sport" : newTeam.sport as! AnyObject,
            "teamName" : newTeam.teamName as! AnyObject
        ]
        Alamofire
            .request(.POST, "http://localhost:8000/api/postTeam?token="+getToken(), parameters: parameters, encoding: ParameterEncoding.JSON)
            .response { request, response, data, error in
                print(response?.statusCode)
                if response?.statusCode == 200{
                    do{
                        if let responseObject = JSON(data:data!).dictionary{
                            let team = responseObject["team"]!.dictionary
                            let teamId = team!["id"]!.int
                            newTeam.id = teamId
                            
                            try self.getManagedContext().save()
                            
                            self.teams.append(newTeam)
                            
                            self.postNewTeamStats(newTeam)
                            
                            self.performSegueWithIdentifier("unwindAddingNewTeam", sender: self)
                        }
                        
                    }catch let error as NSError{
                        print("Could not delete \(error), \(error.userInfo)")
                    }
                }
        }
    }
    
    func postNewTeamStats(newTeam : Teams){
        let entity = NSEntityDescription.entityForName("StatsCategory", inManagedObjectContext: self.getManagedContext())
        let newStats = setNewTeamStats(entity!, newTeam: newTeam)
        
        let parameters = [
            "categories": newStatCategories as AnyObject,
            "teams_id"  : newTeam.id as! AnyObject
        ]
        print(parameters)
        Alamofire
            .request(.POST, "http://localhost:8000/api/postStatsCategory?token="+getToken(), parameters: parameters, encoding: ParameterEncoding.JSON)
            .response { request, response, data, error in
                if response?.statusCode == 200{
                    do{
                        if let responseObject = JSON(data:data!).dictionary{
                            let allStats = responseObject["statsCategory"]!.array
                            for newStat in newStats{
                                
                                let getStatToSetId = allStats!.filter({[newStat.category!].contains(($0["category"].object as? String)!)})
                                
                                for setNewStatId in getStatToSetId{
                                    newStat.id = setNewStatId["id"].int
                                }
                            }
                            try self.getManagedContext().save()
                        }
                    } catch let error as NSError{
                        print("Could not save new stats \(error), \(error.userInfo)")
                    }
                }
        }
    }
    func getManagedContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
    }
    
    func setNewTeam(entity: NSEntityDescription) -> Teams{
        let newTeam = NSManagedObject(entity: entity,
                                      insertIntoManagedObjectContext: self.getManagedContext()) as! Teams
        
        newTeam.setValue(sportName, forKey: "sport")
        newTeam.setValue(teamName, forKey: "teamName")
        newTeam.setValue((self.user?.email)!, forKey: "user_email")
        
        return newTeam
    }
    
    func setNewTeamStats(entity : NSEntityDescription, newTeam: Teams) -> [StatsCategory]{
        var newStatCategoryObjects = [StatsCategory]()

        for newStatCategory in newStatCategories{
            let newTeamStats = NSManagedObject(entity: entity, insertIntoManagedObjectContext: self.getManagedContext()) as! StatsCategory
            newTeamStats.setValue(newStatCategory, forKey: "category")
            newTeamStats.setValue(newTeam.id, forKey: "team_id")
            newStatCategoryObjects.append(newTeamStats)
        }
        return newStatCategoryObjects
    }
    func getToken() ->String{
        if let token = A0SimpleKeychain().stringForKey("userTokenKey"){
            return token
        }
        let noTokenFound = "000"
        return noTokenFound
    }

    //MARK: Segue section
    @IBAction func unwindToAddTeamsSegue(sender: UIStoryboardSegue){
        if let sourceViewController = sender.sourceViewController as? AddTeamNameViewController{
        teamName = sourceViewController.teamNameTextField.text
            updateTable(teamName!, row: 0)
        }
        
        if let sourceViewController = sender.sourceViewController as? SelectTeamTypeTableViewController{
            sportName = sourceViewController.selectedSport
            print(sportName)
            updateTable(sportName!, row: 1)
        }
        if let sourceViewController = sender.sourceViewController as? AddOtherSportViewController{
            sportName = sourceViewController.otherSportTextField.text
            updateTable(sportName!, row: 1)
        }
        if let sourceViewController = sender.sourceViewController as? AddTeamStatsViewController{
            newStatCategories = sourceViewController.statCategoryNames
            updateTable(String(newStatCategories.count), row: 2)
        }
    }
    
    func updateTable(updateField : String, row: Int){
        tableView.beginUpdates()
        let newIndexPath = NSIndexPath(forRow: row, inSection: 0)
        let cellIdentifier = "tableCellOptionsIdentifier"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? OptionsTableViewCell
        cell?.selectedInfo.hidden = false
        cell?.selectedInfo.text = updateField
        cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
        tableView.reloadRowsAtIndexPaths([newIndexPath], withRowAnimation: .Automatic)
        tableView.endUpdates()
    }
}
