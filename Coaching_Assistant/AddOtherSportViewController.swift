//
//  AddOtherSportViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class AddOtherSportViewController: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var otherSportLabel: UILabel!
    @IBOutlet weak var otherSportTextField: UITextField!
    var sport : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let rightButtonPressed = #selector(AddOtherSportViewController.rightButtonPressed)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: rightButtonPressed)
        // Do any additional setup after loading the view.
    }
    
    func rightButtonPressed(){
        performSegueWithIdentifier("unwindFromOtherSportToAddTeam", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
