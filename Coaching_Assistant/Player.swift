//
//  Player.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/10/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import Foundation
import CoreData


class Player: NSManagedObject {

    convenience init?(name : String, id : NSNumber) {
        self.init()
        self.name = name
        self.id = id
        
        if(name.isEmpty || id.integerValue <= 0){
            return nil
        }
    }
}
