//
//  TeamsTableViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/14/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SimpleKeychain
import SwiftyJSON

class TeamsTableViewController: UITableViewController {
    //MARK - Properties
    var user : User?
    var teams = [Teams]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for team in teams{
            print("\(team.teamName)")
        }
        /*
        getTeamsFromServer()
        */
        //use the edit button provided by the table view controller
        navigationItem.leftBarButtonItem = editButtonItem()
        
        getTeams()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return teams.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "TeamsTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TeamsTableViewCell
        
        
        // Configure the cell...
        let team = teams[indexPath.row]
        
        cell.txtTeamName.text = team.valueForKey("teamName")! as! String
        
        return cell
    }
    /*
     override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     performSegueWithIdentifier("selectTeamCellSegue", sender: indexPath.row)
     
     }
     */
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            deleteTeam(indexPath.row)
            
            // remove the deleted team from the array of team objects
            teams.removeAtIndex(indexPath.row)
            
            // remove the deleted team from the table
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    //MARK: - Actions
    /*
    @IBAction func addTeamButton(sender: UIBarButtonItem) {
        createAddTeamAlert()
    }
    */
    
    
    //MARK: - Core Data functions
    func deleteTeam(row : NSInteger){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        managedContext.deleteObject(teams[row])
        do{
            try managedContext.save()
        }
        catch let error as NSError{
            print("Could not delete \(error), \(error.userInfo)")
        }
    }
    
    func getManagedContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
    }
    
    func setNewTeam(entity: NSEntityDescription, teamName: String) -> Teams{
        let newTeam = NSManagedObject(entity: entity,
                                      insertIntoManagedObjectContext: self.getManagedContext()) as! Teams
        
        newTeam.setValue("Volleyball", forKey: "sport")
        newTeam.setValue(teamName, forKey: "teamName")
        newTeam.setValue((self.user?.email)!, forKey: "user_email")
        
        return newTeam
    }
    //gets a list of all teams created by this user
    func getTeams(){

        let fetchRequest = NSFetchRequest(entityName: "Teams")
        
        let teamsPredicate = NSPredicate(format: "user_email == %@", user!.email!)
        fetchRequest.predicate = teamsPredicate
        
        do{
            teams =  try self.getManagedContext().executeFetchRequest(fetchRequest)  as! [Teams]
            
            if teams.count == 0{
                performSegueWithIdentifier("addTeamSegue", sender: self)
            }
        } catch let error as NSError{
            print("Cound not fetch \(error), \(error.userInfo)")
        }
    }
    
    // MARK: - Navigation
    /*
    func createAddTeamAlert(){
        
        let mustAddTeamAlert = UIAlertController(title: "Create A New Team", message: "Enter your team name!", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        mustAddTeamAlert.addTextFieldWithConfigurationHandler({
            (teamName : UITextField!) in
            teamName.placeholder = "Enter team name"
        })
        
        let action = setAddTeamAlert(mustAddTeamAlert)
        mustAddTeamAlert.addAction(action)
        
        self.presentViewController(mustAddTeamAlert, animated: true, completion: nil)
    }
    */
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "selectTeamCellSegue" {
            let playersTableViewController = segue.destinationViewController as? PlayersTableViewController
            if let selectedTeamCell = sender as? TeamsTableViewCell{
                let indexPath = tableView.indexPathForCell(selectedTeamCell)
                let selectedTeam = teams[indexPath!.row]
                print(selectedTeam)
                playersTableViewController!.team = selectedTeam
            }
        }else if segue.identifier == "addTeamSegue"{
            if let user = self.user{
                let navController = segue.destinationViewController as! UINavigationController
                let addTeamTableVC = navController.topViewController as! AddTeamTableViewController
                addTeamTableVC.user = user
                addTeamTableVC.teams = teams
                print("going to addTeam")
            }
        }
    }
    
    //MARK: - Alerts
    /*
    func setAddTeamAlert(mustAddTeamAlert : UIAlertController) -> UIAlertAction {
        let action = UIAlertAction(title: "Submit",
                                   style: UIAlertActionStyle.Default,
                                   handler: {[weak self]
                                    (paramAction:UIAlertAction!) in
                                    if let textFields = mustAddTeamAlert.textFields{
                                        let theTextFields = textFields as [UITextField]
                                        let enteredText = theTextFields[0].text
                                        
                                        do{
                                            let entity =  NSEntityDescription.entityForName("Teams",
                                                inManagedObjectContext:self!.getManagedContext())
                                            
                                            let newTeam = self!.setNewTeam(entity!, teamName: enteredText!)
                                            
                                            self!.postNewTeam(newTeam)
                                            
                                        }catch let error as NSError{
                                            print("error \(error) has \(error.userInfo)")
                                        }
                                    }
            })
        return action
    }
 */
    func appendNewTeamToTable(){
        tableView.beginUpdates()
        let newIndexPath = NSIndexPath(forRow: teams.count-1, inSection: 0)
        tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Automatic)
        tableView.endUpdates()
    }
    
    //MARK: - Alamofire http request
        /*
    func postNewTeam(newTeam : Teams){
        let parameters = [
            "email" : newTeam.user_email as! AnyObject,
            "sport" : newTeam.sport as! AnyObject,
            "teamName" : newTeam.teamName as! AnyObject
        ]
        Alamofire
            .request(.POST, "http://localhost:8000/api/postTeam?token="+getToken(), parameters: parameters, encoding: ParameterEncoding.JSON)
            .response { request, response, data, error in
                print(response?.statusCode)
                if response?.statusCode == 200{
                    do{
                        if let responseObject = JSON(data:data!).dictionary{
                            let team = responseObject["team"]!.dictionary
                            let teamId = team!["id"]!.int
                            newTeam.id = teamId
                            
                            try self.getManagedContext().save()
                            
                            self.teams.append(newTeam)
                            
                            self.appendNewTeamToTable()
                        }
                        
                    }catch let error as NSError{
                        print("Could not delete \(error), \(error.userInfo)")
                    }
                }
        }
    }
 */
    func getTeamsFromServer(){
        let parameters = [
            "email": (user?.email)! as String
        ]
        let token = A0SimpleKeychain()
        
        Alamofire.request(.GET, "http://localhost:8000/api/getTeams?token="+token.stringForKey("userTokenKey")!, parameters: parameters)
            .response { request, response, data, error in
                
                
                if response?.statusCode == 200 {
                    print("success")
                }
        }
    }
    func getToken() ->String{
        if let token = A0SimpleKeychain().stringForKey("userTokenKey"){
            return token
        }
        let noTokenFound = "000"
        return noTokenFound
    }
    //MARK: Segue section
    @IBAction func unwindToTeamsSegue(sender: UIStoryboardSegue){
        if let sourceViewController = sender.sourceViewController as? AddTeamTableViewController{
            print("coming back from add team")
            if teams.count != sourceViewController.teams.count{
                teams = sourceViewController.teams
                print(teams)
                appendNewTeamToTable()
            }
        }
    }
}
