//
//  StatsCategoryCollectionViewCell.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/21/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class StatsCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var statCategoryName: UILabel!
}
