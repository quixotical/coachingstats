//
//  Teams+CoreDataProperties.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/29/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Teams {

    @NSManaged var id: NSNumber?
    @NSManaged var sport: String?
    @NSManaged var teamName: String?
    @NSManaged var user_email: String?
    @NSManaged var users: User?

}
