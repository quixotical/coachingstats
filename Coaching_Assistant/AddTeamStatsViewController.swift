//
//  AddTeamStatsViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/22/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import CoreData

class AddTeamStatsViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    //MARK: - Properties
    var teamStats = [StatsCategory]()
    var statCategoryNames = [String]()
    var managedContext : NSManagedObjectContext?
    @IBOutlet weak var newStatsEntered: UITextField!
    @IBOutlet var teamCategoryViewCollection: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.teamCategoryViewCollection!.dataSource = self
        self.teamCategoryViewCollection!.delegate = self
        
        let rightBarButtonAction = #selector(AddTeamStatsViewController.submitNewStats)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: rightBarButtonAction)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView,
                                   numberOfItemsInSection section: Int) -> Int{
        return statCategoryNames.count
    }
    
    func collectionView(collectionView: UICollectionView,
                          cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("statsCollectionCell", forIndexPath: indexPath) as! StatsCategoryCollectionViewCell

        cell.statCategoryName.textColor = UIColor.whiteColor()
        cell.statCategoryName.textAlignment = NSTextAlignment.Center
        cell.statCategoryName.text = statCategoryNames[indexPath.row]

        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    //MARK: - Actions
    @IBAction func addNewStat(sender: AnyObject) {
        statCategoryNames.append(newStatsEntered.text!)
        newStatsEntered.text = ""
        teamCategoryViewCollection.reloadData()
    }
    
    func submitNewStats(){
        performSegueWithIdentifier("unwindToAddTeamFromAddStats", sender: self)
    }
    
    func getManagedContext() -> NSManagedObjectContext{
        if managedContext == nil{
            let appDelegate =
                UIApplication.sharedApplication().delegate as! AppDelegate
            
            managedContext = appDelegate.managedObjectContext
            return managedContext!
        }
        return managedContext!
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
