//
//  AddTeamOptionsViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class AddTeamNameViewController: UIViewController {
    //MARK: - Properties
    var selectedOption: String?
    
    let nameOption = "Team Name"
    let categoryOption = "Category Option"
    let statsToTrackOption = "Stats to Track"
    
    @IBOutlet weak var directionsLabel: UILabel!
    @IBOutlet weak var teamNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = selectedOption
        
        let rightButtonSelector = #selector(AddTeamNameViewController.submitName)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: rightButtonSelector)
        
        loadOptions()
        // Do any additional setup after loading the view.
    }
    
    func submitName(){
        performSegueWithIdentifier("unwindToAddTeamFromAddName", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadOptions(){
        if selectedOption == nameOption{
            directionsLabel.text = "Enter Your New Team Name"
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "unwindToAddTeamFromAddName"{
            
        }
    }
 

}
