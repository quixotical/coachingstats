//
//  TeamsTableViewCell.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/29/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class TeamsTableViewCell: UITableViewCell {
    //MARK: - Properties
    @IBOutlet weak var txtTeamName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
