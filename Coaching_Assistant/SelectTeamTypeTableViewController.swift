//
//  SelectCategoryTableViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class SelectTeamTypeTableViewController: UITableViewController {
    //MARK: - Properties
    var sportOptions = []
    var selectedSport: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select Sport"
        loadDefaultSportOptions()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDefaultSportOptions(){
        sportOptions = ["Volleyball", "Basketball", "Soccer", "Football", "Other"]
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sportOptions.count
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        switch sportOptions[indexPath.row] as! String {
        case "Other":
            let otherSportIdentifier = "addOtherSport"
            let otherSportVC = self.storyboard?.instantiateViewControllerWithIdentifier(otherSportIdentifier) as? AddOtherSportViewController
            self.navigationController?.pushViewController(otherSportVC!, animated: true)
        default:
            selectedSport = sportOptions[indexPath.row] as! String
            print(selectedSport)
            performSegueWithIdentifier("unwindWithTeamType", sender: self)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "sportTableCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as? SportTableViewCell

        // Configure the cell...
        let sportOption = sportOptions[indexPath.row]
        if sportOptions[indexPath.row] as! String == "Other" {
            cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        cell?.sportLabel.text = sportOption as? String
        
        return cell!
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }

}
