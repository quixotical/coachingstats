//
//  PlayerStatsViewController.swift
//  Volleyball_Stats_Tracker
//
//  Created by Jacob Boomgaarden on 4/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import Charts

class PlayerStatsViewController: UIViewController {
    
    //Mvar: Properties
    @IBOutlet weak var barChartView: BarChartView!
    var months: [String]!
    var team : Teams?
    var player : Player?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let addStatsSelector = Selector("addNewStats")
        let addNewStats = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: addStatsSelector)
        self.navigationItem.rightBarButtonItem = addNewStats
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let unitsSold = [20.0, 22.2, 30.3, 25.3, 19.1, 23.3, 25.0, 29.3, 22.2, 11.0, 23.2, 31.9]
        
        setChart(months, values: unitsSold)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Chart Setup
    func setChart(dataPoints: [String], values: [Double]){
        barChartView.noDataText = "You need to provide data for the chart"
        barChartView.descriptionText = "Players Stats"
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        
        chartDataSet.colors = ChartColorTemplates.joyful()
        
        let limitLine = ChartLimitLine(limit: 20, label: "Target Sales")
        barChartView.rightAxis.addLimitLine(limitLine)
        
//        barChartView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1)
        barChartView.xAxis.labelPosition = .Bottom
        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInOutBounce)
        barChartView.data = chartData
        
        
    }
    
    
    // MARK: - Navigation
    func addNewStats(){
        super.performSegueWithIdentifier("newStatsSegue", sender: self)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "newStatsSegue"{
            let newStatsVC = segue.destinationViewController as? AddNewPlayerStatsViewController
            if var team = self.team{
                newStatsVC?.team = team
            }
        }
    }
}
