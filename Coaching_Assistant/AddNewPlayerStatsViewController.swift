//
//  AddNewPlayerStatsViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/14/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import CoreData

class AddNewPlayerStatsViewController: UIViewController {
    var team : Teams?
    var managedContext : NSManagedObjectContext?
    var teamStatsCategories = [StatsCategory]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Player Stats"
        // Do any additional setup after loading the view.
        
        getTeamStatsCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - CoreData functions
    func getTeamStatsCategories(){
        let fetchRequest = NSFetchRequest(entityName: "StatsCategory")
        let teamIdString = team!.id as? AnyObject
        print(team)
        let statsPredicate = NSPredicate(format: "team_id == %@", argumentArray: [teamIdString!])
        
        fetchRequest.predicate = statsPredicate
        
        do{
            teamStatsCategories = try self.getManagedContext().executeFetchRequest(fetchRequest) as! [StatsCategory]
            print(teamStatsCategories)
        }catch let error as NSError{
            print("Error fetching team stat categories \(error), \(error.userInfo)")
        }
    }
    
    func getManagedContext() -> NSManagedObjectContext{
        if managedContext == nil{
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            managedContext = appDelegate.managedObjectContext
            return managedContext!
        }else{
            return managedContext!
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
