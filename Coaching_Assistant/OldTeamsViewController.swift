//
//  TeamsViewController.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/14/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import CoreData
import SimpleKeychain
import Alamofire
import SwiftyJSON

class TeamsViewController: UIViewController {
    //MARK - Properties
    var user : User?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let token = A0SimpleKeychain()
        Alamofire.request(.GET, "http://localhost:8000/api/users?token="+token.stringForKey("userTokenKey")!)
            .response{ request, response, data, error in
                
                let res = JSON(data:data!).dictionary
        }

        getTeams()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTeams(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Teams")
        
        let teamsPredicate = NSPredicate(format: "user_email == %@", user!.email!)
        fetchRequest.predicate = teamsPredicate
        
        do{
            let fetchTeams =  try managedContext.executeFetchRequest(fetchRequest)  as! [NSManagedObject]
            
            if fetchTeams.count > 0{
                let team = (fetchTeams[0] as? Team)!
                
            }else{
                
                
                
                
            }
        } catch let error as NSError{
            print("Cound not fetch \(error), \(error.userInfo)")
        }
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
