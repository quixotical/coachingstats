//
//  Teams.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 5/29/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import Foundation
import CoreData


class Teams: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    convenience init?(sport: String, id: NSNumber, teamName: String, user_email: String, users: User){
        self.init()
        self.sport = sport
        self.id = id
        self.teamName = teamName
        self.user_email = user_email
        self.users = users
        
        if user_email.isEmpty || id.integerValue <= 0 || sport.isEmpty || teamName.isEmpty{
            return nil
        }
    }
}
