//
//  PlayersTableViewCell.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/15/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit

class PlayersTableViewCell: UITableViewCell {
    //MARK: - Properties
    @IBOutlet weak var playerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
