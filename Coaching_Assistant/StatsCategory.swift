//
//  StatsCategory.swift
//  Coaching_Assistant
//
//  Created by Jacob Boomgaarden on 6/21/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import Foundation
import CoreData


class StatsCategory: NSManagedObject {

    convenience init?(category: String, id: NSNumber, team_id: NSNumber){
        self.init()
        self.category = category
        self.id = id
        self.team_id = team_id
        
        if(category.isEmpty || id.integerValue <= 0 || team_id.integerValue <= 0){
            return nil
        }
    }
}
