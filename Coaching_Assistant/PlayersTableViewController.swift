//
//  PlayersTableViewController.swift
//  Volleyball_Stats_Tracker
//
//  Created by Jacob Boomgaarden on 4/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SimpleKeychain
import SwiftyJSON

class PlayersTableViewController: UITableViewController{
    
    //MARK: Properties
    var managedContext : NSManagedObjectContext? = nil
    var team : Teams?
    var players = [Player]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUIBarButtons()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUIBarButtons(){
        let addPlayerSelector : Selector = "addNewPlayer"
        let addPlayer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: addPlayerSelector)
        self.navigationItem.rightBarButtonItem = addPlayer

    }
    /*
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){

    }
     */
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return players.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "PlayersTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PlayersTableViewCell
        // Configure the cell...
        let person = players[indexPath.row]
        
        cell.playerName!.text = person.valueForKey("name") as! String?
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: - Actions
    
    @IBAction func addNewPlayer(){
        let addPlayerAlertMessage = createAddPlayerAlert()
        //TODO - figure out how to connect the addPlayerAlertMessage we made with the add player button in navigation
        self.presentViewController(addPlayerAlertMessage, animated: true, completion: nil)

    }
    
    func createAddPlayerAlert() -> UIAlertController{
        let addPlayerAlert = UIAlertController(title: "Add Player", message:"Enter the new players name",
                                               preferredStyle: UIAlertControllerStyle.Alert)
        addPlayerAlert.addTextFieldWithConfigurationHandler({
            (textField : UITextField!) in
            textField.placeholder = "Enter players name"
        })
        
        let action = setAddPlayerAlert(addPlayerAlert)
        addPlayerAlert.addAction(action)
        
        return addPlayerAlert
        
    }
    
    func setAddPlayerAlert(addPlayerAlert : UIAlertController) -> UIAlertAction{
        let action = UIAlertAction(title: "Submit", style: UIAlertActionStyle.Default,
                                   handler: {[weak self] (paramAction: UIAlertAction!) in
                                    if let textFields = addPlayerAlert.textFields{
                                        let allTextArray = textFields as [UITextField]
                                        let playerName = allTextArray[0].text
                                        
                                        let entity = NSEntityDescription.entityForName("Player", inManagedObjectContext: self!.getManagedContext())
                                        
                                        let newPlayer = self!.setNewPlayer(entity!, playerName: playerName!)
                                        
                                        self!.postNewPlayer(newPlayer)
                                    }
            })
        return action
    }
    
    func setNewPlayer(entity: NSEntityDescription, playerName: String)->Player{
        let newPlayer = NSManagedObject(entity: entity, insertIntoManagedObjectContext: getManagedContext()) as? Player
        
        newPlayer!.name = playerName
        return newPlayer!
    }
    func getManagedContext() -> NSManagedObjectContext{
        if managedContext == nil{
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            managedContext = appDelegate.managedObjectContext
            return managedContext!
        }else{
            return managedContext!
        }
    }
    
    func updateTable(){
        self.tableView.beginUpdates()
        let indexPath = NSIndexPath(forRow: self.players.count - 1, inSection: 0)
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        self.tableView.endUpdates()

    }
    // MARK: CoreData fetch
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managerContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Player")
        
        do{
            let results = try managerContext.executeFetchRequest(fetchRequest)
            
            players = results as! [Player]
            if players.count == 0{
                addNewPlayer()
            }
        }catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func postNewPlayer(newPlayer : Player){
        let parameters = [
            "teamMemberName" : newPlayer.name as! AnyObject,
            "teamId" : team?.id as! AnyObject
        ]
        print(getToken())
        Alamofire
            .request(.POST, "http://localhost:8000/api/postTeamMember?token="+getToken(), parameters: parameters, encoding: .JSON)
            .response{ request, response, data, error in
                print(response?.statusCode)
                if response?.statusCode == 200{
                    do{
                        let responseObject = JSON(data:data!).dictionary
                        let teamMember = responseObject!["teamMember"]!.dictionary
                        newPlayer.id = teamMember!["id"]!.int!
                        try self.getManagedContext().save()
                        self.players.append(newPlayer)
                        self.updateTable()
                    }catch let error as NSError{
                        print("Could not save locally \(error), \(error.userInfo)")
                    }
                }
        }
    }
    
    func getToken() ->String{
        if let token = A0SimpleKeychain().stringForKey("userTokenKey"){
            return token
        }
        let noTokenFound = "000"
        return noTokenFound
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToPlayerStats"{
            let playerStatsVC = segue.destinationViewController as! PlayerStatsViewController
            if let selectedPlayerCell = sender as? UITableViewCell{
                let indexPath = tableView.indexPathForCell(selectedPlayerCell)
                let selectedPlayer = players[(indexPath?.row)!]
                playerStatsVC.team = team
                playerStatsVC.player = selectedPlayer
            }
        }
    }
 

}
