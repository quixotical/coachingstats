//
//  NewUserViewController.swift
//  Volleyball_Stats_Tracker
//
//  Created by Jacob Boomgaarden on 4/17/16.
//  Copyright © 2016 Jacob Boomgaarden. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SimpleKeychain
import CoreData

class NewUserViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var reEnterPassword: UITextField!
    var mUser : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func submitButton(sender: UIButton) {
        let parameters = [
                "email": email.text as! AnyObject,
                "password": password.text as! AnyObject
            ]
        Alamofire.request(.POST, "http://localhost:8000/api/register", parameters: parameters, encoding: .JSON)
            .response{ request, response, data, error in
                
                //if user creation is a success
                if response!.statusCode == 200{
                    
                    //retrieve the useres unique session token from the server
                    let str = JSON(data: data!).dictionary
                    let returnData = str!["data"]?.dictionary
                    let token = returnData!["token"]?.string
                    let newUser = returnData!["user"]?.dictionary
                    print(newUser)
                    //Save the session token to the keychain
                    let instance = A0SimpleKeychain()
                    
                    instance.setString(token!, forKey:"userTokenKey")
                    //save the new user locally before sending them to welcome screen
                    self.storeNewUser(newUser!)
                    
                    //send the user from the new user screen to the main screen
                    let storyBoard = UIStoryboard(name: "Main", bundle:nil)
                    let teamView = storyBoard.instantiateViewControllerWithIdentifier("teamsView")
                    let navigationController = UINavigationController(rootViewController: teamView)
                    
                    super.performSegueWithIdentifier("newUserToTeamsSegue", sender: self)
                    super.presentViewController(navigationController, animated: true, completion: nil)
                    
                //user already exists
                }else if response!.statusCode == 401{
                    let userExistsAlert = UIAlertController(title: "Oops!", message: "It looks like there is already an account registered to this email", preferredStyle: UIAlertControllerStyle.Alert)
                    userExistsAlert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler:nil))
                    self.presentViewController(userExistsAlert, animated: true, completion: nil)
                //Some other server error
                }else{
                    let userCreationFailure = UIAlertController(title: "Server Error", message: "Unable to make successful connection to server", preferredStyle: UIAlertControllerStyle.Alert)
                    userCreationFailure.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler:nil))
                    self.presentViewController(userCreationFailure, animated: true, completion: nil)
                }
        }
    }
    func storeNewUser(newUser: [String:JSON]){
        //create app delegate to give context for coredata functions we will call
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        //create a managed context to create a "scratchPad" which holds our entities we want to use
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("User",
                                                        inManagedObjectContext:managedContext)
        
        let newUserManagedObject = NSManagedObject(entity: entity!,
                                      insertIntoManagedObjectContext: managedContext)
        //3
        newUserManagedObject.setValue(newUser["email"]?.string, forKey: "email")
        newUserManagedObject.setValue(newUser["id"]?.int, forKey: "id")
        newUserManagedObject.setValue(true, forKey: "sessionExists")
        
        mUser = newUserManagedObject as? User
        //4
        do {
            try managedContext.save()
            //5
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        print(segue.identifier)
        if segue.identifier == "newUserToTeamsSegue" && mUser != nil {
            let navController = segue.destinationViewController as! UINavigationController
            let welcomeView = navController.topViewController as! TeamsTableViewController
            welcomeView.user = mUser
        }
    }
 

}
